﻿using Fleck;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace WebSockets.Models
{
    public static class WebSockets
    {
        public static string StartServer()
        {
            var server = new WebSocketServer("ws://"+ IPAddress.Any +":443");
			try
			{
				server.Start(socket =>
				{
					socket.OnOpen = () => Console.WriteLine("Open!");
					socket.OnClose = () => Console.WriteLine("Close!");
					socket.OnMessage = message => socket.Send(message);
				});
				return String.Format("Connection str: {0}, Listener: {1}, port: {2}", server.Location, server.ListenerSocket, server.Port);
				//return "Ok!";
			}
			catch (Exception err)
			{
				return err.Message;
			}
        }

		public static string GetInfo()
		{
			string DomainName = HttpContext.Current.Request.Url.Host;
			IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
			IPAddress ipAddress = ipHostInfo.AddressList[0];

			return String.Format("host: {0}, ip: {1}", DomainName, ipAddress.ToString());
		}
    }
}