﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace WebSockets.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

		public ActionResult StartServer()
		{
			return Json(Models.WebSockets.StartServer(), JsonRequestBehavior.AllowGet);
		}

		public ActionResult GetInfo()
		{
			return Json(Models.WebSockets.GetInfo(), JsonRequestBehavior.AllowGet);
		}
    }
}
